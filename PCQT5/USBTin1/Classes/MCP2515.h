/*
 * MCP2515.h
 *
 *  Created on: 5 avr. 2016
 *      Author: michel
 */

#ifndef MCP2515_H_
#define MCP2515_H_
#include "SerialPort.h"
class MCP2515Reg {
	int valregistre;
public:
	MCP2515Reg();
	virtual ~MCP2515Reg();
	void setRegistre(unsigned char);
	unsigned char getRegistre();

};

#endif /* MCP2515_H_ */
