/**
 *  Copyright (C) 2016  michel  (michel@btssn.delattre.com)
 *  @file         USBTin.h
 *  @brief        
 *  @version      0.1
 *  @date         30 mars 2016 09:40:52
 *
 *  @note         Voir la description detaillee explicite dans le fichier
 *                USBTin.cpp
 *                C++ Google Style :
 *                http://google-styleguide.googlecode.com/svn/trunk/cppguide.xml
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _USBTIN_H
#define _USBTIN_H
#include "RegMCP2515Dlg.h"

// Includes system C
//#include <stdint.h>  // definition des types int8_t, int16_t ...

// Includes system C++

// Includes qt
#include <QMainWindow>

// Includes application
#include "ui_USBTin.h"
#include "BusCAN.h"
#include <QStandardItemModel>
#include "device.h"
#include "MCP2515.h"
// Constantes
// ex :
// const int kDaysInAWeek = 7;

// Enumerations
// ex :
// enum Couleur
// {
//     kBlue = 0,
//     kWhite,
//     kRed,
// };

// Structures
// ex:
// struct UrlTableProperties
// {
//  string name;
//  int numEntries;
// }

// Declarations de classe avancees
// ex:
// class MaClasse;

/** @brief 
 *  Description detaillee de la classe.
 */
class USBTin:public QMainWindow, private Ui_USBTin
{
	Q_OBJECT
	RegMCP2515Dlg *mreg;
	SerialPort *mport;
	MCP2515Reg *mcp;
	int numeromessage;
	BusCAN *mcan;
	bool opencan;
	bool openport;
	QStandardItemModel *modelePlug_in;
	bool checkextended;
	bool checkrtr;
	Device *mondevice;
	int mode;
public :
    /**
     * Constructeur
     */
    USBTin();
    /**
     * Destructeur
     */
    ~USBTin();

    // Methodes publiques de la classe
    // ex : ReturnType NomMethode(Type);

    // Pour les associations :
    // Methodes publiques setter/getter (mutateurs/accesseurs) des attributs prives
    // ex :
    // void setNomAttribut(Type nomAttribut) { nomAttribut_ = nomAttribut; }
    // Type getNomAttribut(void) const { return nomAttribut_; }

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives
    // ex :
    // Type nomAttribut_;

    // Methodes privees
signals:
	void readConfirm();
	void versMCP2515Reg(unsigned char);
// signaux générés    
private slots:
    // definition des slots privées

	void onConnecter();
	void onOuvrir();
	void onDeconnect();
	void onEnvoie();
	void onRecu();
	void onEffacer();
	void onSauvegarder();
	void onConfirm();
	void onCheckExtended();
	void onRTR();
	void onMCP2515();
	void onAcceptanceMask();
	void onAcceptanceFiltre();
	void onVitesse(int index);
	void slotOpenCAN();
	void slotCloseCAN();
	void slotVitesseCAN();
	void onDeconnectS();
	void slotLireMCP2515(unsigned char reg);
private slots:
     // definition des slots publics
    
};

// Methodes publiques inline
// ex :
// inline void USBTin::maMethodeInline(Type valeur)
// {
//   
// }
// inline Type USBTin::monAutreMethode_inline_(void)
// {
//   return 0;
// }

#endif  // _USBTIN_H

