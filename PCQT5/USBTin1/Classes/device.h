/*
 * device.h
 *
 *  Created on: 2 avr. 2016
 *      Author: michel
 */

#ifndef DEVICE_H_
#define DEVICE_H_
#include <QVector>
#include <QObject>

class Device: public QObject {
	 Q_OBJECT

	bool devicetrouve;
	QVector<QString> dev;
public:
	Device();
	virtual ~Device();
	 bool getDevices(QVector<QString> *);
	//bool deviceTrouve();
};

#endif /* DEVICE_H_ */
