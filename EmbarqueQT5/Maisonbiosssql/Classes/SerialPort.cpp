/*
 * SerialPort.cpp
 *
 *  Created on: 9 févr. 2015
 *      Author: michel
 */

#include "SerialPort.h"
//#include <QDebug>

SerialPort::SerialPort(QObject *parent) :
QSerialPort(parent) {

}

bool SerialPort::setOpenPort(QString *port){
	this->setPortName(*port);
	this->close();
	this->setBaudRate(115200);
	if (!this->isOpen())
		openport=this->open(QIODevice::ReadWrite);
	else openport=true;
	this->setBaudRate(115200);
	return openport;
}

SerialPort::~SerialPort() {
	// TODO Auto-generated destructor stub
	this->flush();
	this->close();
}

unsigned char SerialPort::getNextByte(){
	char data;
 this->readData(&data,1);
 return (unsigned char)(data);
}


int SerialPort::writeBuf(char *buf,int l){
//	return this->writeBuffer(*buffer);
//	char *d=new char[buffer->length()];
/*	int j=0;
	for (int i=0;i<buffer->length();i++){
		j+=this->writeData(buffer[i],1);
	}
*
	/*for (int i=0;i<buffer->length();i++){
		d[i]=(char)(*buffer[i]);
	}
	return writeData(d,buffer->length());
	*/
	int j=this->writeData(buf,l);
	return j;
}


bool SerialPort::getOpen(){
	return openport;
}
