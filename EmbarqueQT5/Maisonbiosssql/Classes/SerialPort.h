/*
 * SerialPort.h
 *
 *  Created on: 9 févr. 2015
 *      Author: michel
 */

#ifndef SERIALPORT_H_
#define SERIALPORT_H_
#include <QtSerialPort>

class SerialPort:public QSerialPort {
	 Q_OBJECT
	 bool openport;
public:
	SerialPort(QObject *parent = 0);
	virtual ~SerialPort();
    unsigned char getNextByte();
    bool setOpenPort(QString *port);
    // return number of bytes in receive buffer
  //  unsigned int bytesAvailable();
    bool getOpen();
    // write buffer
//    int writeBuf(QByteArray *buffer);
    int writeBuf( char *buf,int l   );

 //   int OuvrirPort();

    QByteArray *dataBuffer;

};

#endif /* SERIALPORT_H_ */
