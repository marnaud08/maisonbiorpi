/********************************************************************************
** Form generated from reading UI file 'MaisonBioDlg.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAISONBIODLG_H
#define UI_MAISONBIODLG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MaisonBioDlg
{
public:
    QWidget *centralwidget;
    QTabWidget *onglets;
    QWidget *ong_donnees;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents;
    QGroupBox *Panneau1;
    QWidget *formLayoutWidget;
    QFormLayout *Panneau1_form;
    QLabel *intensitLabel_1;
    QLineEdit *intensitLineEdit_1;
    QLabel *tensionLabel_1;
    QLineEdit *tensionLineEdit1;
    QLabel *energieLabel_1;
    QLineEdit *energieLineEdit_1;
    QLabel *temperatureLabel_1;
    QLineEdit *temperatureLineEdit_1;
    QGroupBox *Panneau5;
    QWidget *formLayoutWidget_2;
    QFormLayout *Panneau5_form;
    QLabel *intensitLabel_5;
    QLineEdit *intensitLineEdit_5;
    QLabel *tensionLabel_5;
    QLineEdit *tensionLineEdit_5;
    QLabel *energieLabel_5;
    QLineEdit *energieLineEdit_5;
    QLabel *temperatureLabel_5;
    QLineEdit *temperatureLineEdit_5;
    QGroupBox *Panneau9;
    QWidget *formLayoutWidget_3;
    QFormLayout *Panneau9_form;
    QLabel *intensitLabel_9;
    QLineEdit *intensitLineEdit_9;
    QLabel *tensionLabel_9;
    QLineEdit *tensionLineEdit_9;
    QLabel *energieLabel_9;
    QLineEdit *energieLineEdit_9;
    QLabel *temperatureLabel_9;
    QLineEdit *temperatureLineEdit_9;
    QGroupBox *Panneau13;
    QWidget *formLayoutWidget_4;
    QFormLayout *Panneau13_form;
    QLabel *intensitLabel_13;
    QLineEdit *intensitLineEdit_13;
    QLabel *tensionLabel_13;
    QLineEdit *tensionLineEdit_13;
    QLabel *energieLabel_13;
    QLineEdit *energieLineEdit_13;
    QLabel *temperatureLabel_13;
    QLineEdit *temperatureLineEdit_13;
    QGroupBox *Panneau6;
    QWidget *formLayoutWidget_8;
    QFormLayout *Panneau6_form;
    QLabel *intensitLabel_6;
    QLineEdit *intensitLineEdit_6;
    QLabel *tensionLabel_6;
    QLineEdit *tensionLineEdit_6;
    QLabel *energieLabel_6;
    QLineEdit *energieLineEdit_6;
    QLabel *temperatureLabel_6;
    QLineEdit *temperatureLineEdit_6;
    QGroupBox *Panneau2;
    QWidget *formLayoutWidget_6;
    QFormLayout *Panneau2_form;
    QLabel *intensitLabel_2;
    QLineEdit *intensitLineEdit_2;
    QLabel *tensionLabel_2;
    QLineEdit *tensionLineEdit_2;
    QLabel *energieLabel_2;
    QLineEdit *energieLineEdit_2;
    QLabel *temperatureLabel_2;
    QLineEdit *temperatureLineEdit_2;
    QGroupBox *Panneau10;
    QWidget *formLayoutWidget_5;
    QFormLayout *Panneau10_form;
    QLabel *intensitLabel_10;
    QLineEdit *intensitLineEdit_10;
    QLabel *tensionLabel_10;
    QLineEdit *tensionLineEdit_10;
    QLabel *energieLabel_10;
    QLineEdit *energieLineEdit_10;
    QLabel *temperatureLabel_10;
    QLineEdit *temperatureLineEdit_10;
    QGroupBox *Panneau14;
    QWidget *formLayoutWidget_7;
    QFormLayout *Panneau14_form;
    QLabel *intensitLabel_14;
    QLineEdit *intensitLineEdit_14;
    QLabel *tensionLabel_14;
    QLineEdit *tensionLineEdit_14;
    QLabel *energieLabel_14;
    QLineEdit *energieLineEdit_14;
    QLabel *temperatureLabel_14;
    QLineEdit *temperatureLineEdit_14;
    QGroupBox *Panneau11;
    QWidget *formLayoutWidget_9;
    QFormLayout *Panneau11_form;
    QLabel *intensitLabel_11;
    QLineEdit *intensitLineEdit_11;
    QLabel *tensionLabel_11;
    QLineEdit *tensionLineEdit_11;
    QLabel *energieLabel_11;
    QLineEdit *energieLineEdit_11;
    QLabel *temperatureLabel_11;
    QLineEdit *temperatureLineEdit_11;
    QGroupBox *Panneau3;
    QWidget *formLayoutWidget_10;
    QFormLayout *Panneau3_form;
    QLabel *intensitLabel_3;
    QLineEdit *intensitLineEdit_3;
    QLabel *tensionLabel_3;
    QLineEdit *tensionLineEdit_3;
    QLabel *energieLabel_3;
    QLineEdit *energieLineEdit_3;
    QLabel *temperatureLabel_3;
    QLineEdit *temperatureLineEdit_3;
    QGroupBox *Panneau15;
    QWidget *formLayoutWidget_11;
    QFormLayout *Panneau15_form;
    QLabel *intensitLabel_15;
    QLineEdit *intensitLineEdit_15;
    QLabel *tensionLabel_15;
    QLineEdit *tensionLineEdit_15;
    QLabel *energieLabel_15;
    QLineEdit *energieLineEdit_15;
    QLabel *temperatureLabel_15;
    QLineEdit *temperatureLineEdit_15;
    QGroupBox *Panneau7;
    QWidget *formLayoutWidget_12;
    QFormLayout *Panneau7_form;
    QLabel *intensitLabel_7;
    QLineEdit *intensitLineEdit_7;
    QLabel *tensionLabel_7;
    QLineEdit *tensionLineEdit_7;
    QLabel *energieLabel_7;
    QLineEdit *energieLineEdit_7;
    QLabel *temperatureLabel_7;
    QLineEdit *temperatureLineEdit_7;
    QGroupBox *Panneau12;
    QWidget *formLayoutWidget_13;
    QFormLayout *Panneau12_form;
    QLabel *intensitLabel_12;
    QLineEdit *intensitLineEdit_12;
    QLabel *tensionLabel_12;
    QLineEdit *tensionLineEdit_12;
    QLabel *energieLabel_12;
    QLineEdit *energieLineEdit_12;
    QLabel *temperatureLabel_12;
    QLineEdit *temperatureLineEdit_12;
    QGroupBox *Panneau4;
    QWidget *formLayoutWidget_14;
    QFormLayout *Panneau4_form;
    QLabel *intensitLabel_4;
    QLineEdit *intensitLineEdit_4;
    QLabel *tensionLabel_4;
    QLineEdit *tensionLineEdit_4;
    QLabel *energieLabel_4;
    QLineEdit *energieLineEdit_4;
    QLabel *temperatureLabel_4;
    QLineEdit *temperatureLineEdit_4;
    QGroupBox *Panneau16;
    QWidget *formLayoutWidget_15;
    QFormLayout *Panneau16_form;
    QLabel *intensitLabel_16;
    QLineEdit *intensitLineEdit_16;
    QLabel *tensionLabel_16;
    QLineEdit *tensionLineEdit_16;
    QLabel *energieLabel_16;
    QLineEdit *energieLineEdit_16;
    QLabel *temperatureLabel_16;
    QLineEdit *temperatureLineEdit_16;
    QGroupBox *Panneau8;
    QWidget *formLayoutWidget_16;
    QFormLayout *Panneau8_form;
    QLabel *intensitLabel_8;
    QLineEdit *intensitLineEdit_8;
    QLabel *tensionLabel_8;
    QLineEdit *tensionLineEdit_8;
    QLabel *energieLabel_8;
    QLineEdit *energieLineEdit_8;
    QLabel *temperatureLabel_8;
    QLineEdit *temperatureLineEdit_8;
    QWidget *ong_defauts;
    QTextEdit *defauts;

    void setupUi(QMainWindow *MaisonBioDlg)
    {
        if (MaisonBioDlg->objectName().isEmpty())
            MaisonBioDlg->setObjectName(QStringLiteral("MaisonBioDlg"));
        MaisonBioDlg->resize(679, 383);
        centralwidget = new QWidget(MaisonBioDlg);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        onglets = new QTabWidget(centralwidget);
        onglets->setObjectName(QStringLiteral("onglets"));
        onglets->setGeometry(QRect(0, 0, 721, 381));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(onglets->sizePolicy().hasHeightForWidth());
        onglets->setSizePolicy(sizePolicy);
        onglets->setMinimumSize(QSize(631, 0));
        onglets->setContextMenuPolicy(Qt::NoContextMenu);
        onglets->setStyleSheet(QStringLiteral("QTabBar::tab { height: 50px; width: 318px; }"));
        onglets->setTabShape(QTabWidget::Rounded);
        onglets->setIconSize(QSize(16, 16));
        ong_donnees = new QWidget();
        ong_donnees->setObjectName(QStringLiteral("ong_donnees"));
        scrollArea = new QScrollArea(ong_donnees);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setGeometry(QRect(0, 0, 711, 351));
        sizePolicy.setHeightForWidth(scrollArea->sizePolicy().hasHeightForWidth());
        scrollArea->setSizePolicy(sizePolicy);
        scrollArea->setContextMenuPolicy(Qt::NoContextMenu);
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        scrollArea->setWidgetResizable(false);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 686, 609));
        sizePolicy.setHeightForWidth(scrollAreaWidgetContents->sizePolicy().hasHeightForWidth());
        scrollAreaWidgetContents->setSizePolicy(sizePolicy);
        Panneau1 = new QGroupBox(scrollAreaWidgetContents);
        Panneau1->setObjectName(QStringLiteral("Panneau1"));
        Panneau1->setGeometry(QRect(0, 0, 171, 151));
        Panneau1->setContextMenuPolicy(Qt::NoContextMenu);
        formLayoutWidget = new QWidget(Panneau1);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau1_form = new QFormLayout(formLayoutWidget);
        Panneau1_form->setObjectName(QStringLiteral("Panneau1_form"));
        Panneau1_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau1_form->setLabelAlignment(Qt::AlignCenter);
        Panneau1_form->setVerticalSpacing(2);
        Panneau1_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_1 = new QLabel(formLayoutWidget);
        intensitLabel_1->setObjectName(QStringLiteral("intensitLabel_1"));
        intensitLabel_1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_1);

        intensitLineEdit_1 = new QLineEdit(formLayoutWidget);
        intensitLineEdit_1->setObjectName(QStringLiteral("intensitLineEdit_1"));
        intensitLineEdit_1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_1);

        tensionLabel_1 = new QLabel(formLayoutWidget);
        tensionLabel_1->setObjectName(QStringLiteral("tensionLabel_1"));
        tensionLabel_1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_1);

        tensionLineEdit1 = new QLineEdit(formLayoutWidget);
        tensionLineEdit1->setObjectName(QStringLiteral("tensionLineEdit1"));
        tensionLineEdit1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit1);

        energieLabel_1 = new QLabel(formLayoutWidget);
        energieLabel_1->setObjectName(QStringLiteral("energieLabel_1"));
        energieLabel_1->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_1->setAlignment(Qt::AlignCenter);

        Panneau1_form->setWidget(2, QFormLayout::LabelRole, energieLabel_1);

        energieLineEdit_1 = new QLineEdit(formLayoutWidget);
        energieLineEdit_1->setObjectName(QStringLiteral("energieLineEdit_1"));
        energieLineEdit_1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_1);

        temperatureLabel_1 = new QLabel(formLayoutWidget);
        temperatureLabel_1->setObjectName(QStringLiteral("temperatureLabel_1"));
        temperatureLabel_1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_1);

        temperatureLineEdit_1 = new QLineEdit(formLayoutWidget);
        temperatureLineEdit_1->setObjectName(QStringLiteral("temperatureLineEdit_1"));
        temperatureLineEdit_1->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau1_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_1);

        Panneau5 = new QGroupBox(scrollAreaWidgetContents);
        Panneau5->setObjectName(QStringLiteral("Panneau5"));
        Panneau5->setGeometry(QRect(0, 150, 171, 151));
        Panneau5->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau5->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_2 = new QWidget(Panneau5);
        formLayoutWidget_2->setObjectName(QStringLiteral("formLayoutWidget_2"));
        formLayoutWidget_2->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_2->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau5_form = new QFormLayout(formLayoutWidget_2);
        Panneau5_form->setObjectName(QStringLiteral("Panneau5_form"));
        Panneau5_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau5_form->setLabelAlignment(Qt::AlignCenter);
        Panneau5_form->setVerticalSpacing(2);
        Panneau5_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_5 = new QLabel(formLayoutWidget_2);
        intensitLabel_5->setObjectName(QStringLiteral("intensitLabel_5"));
        intensitLabel_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_5);

        intensitLineEdit_5 = new QLineEdit(formLayoutWidget_2);
        intensitLineEdit_5->setObjectName(QStringLiteral("intensitLineEdit_5"));
        intensitLineEdit_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_5);

        tensionLabel_5 = new QLabel(formLayoutWidget_2);
        tensionLabel_5->setObjectName(QStringLiteral("tensionLabel_5"));
        tensionLabel_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_5);

        tensionLineEdit_5 = new QLineEdit(formLayoutWidget_2);
        tensionLineEdit_5->setObjectName(QStringLiteral("tensionLineEdit_5"));
        tensionLineEdit_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_5);

        energieLabel_5 = new QLabel(formLayoutWidget_2);
        energieLabel_5->setObjectName(QStringLiteral("energieLabel_5"));
        energieLabel_5->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_5->setAlignment(Qt::AlignCenter);

        Panneau5_form->setWidget(2, QFormLayout::LabelRole, energieLabel_5);

        energieLineEdit_5 = new QLineEdit(formLayoutWidget_2);
        energieLineEdit_5->setObjectName(QStringLiteral("energieLineEdit_5"));
        energieLineEdit_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_5);

        temperatureLabel_5 = new QLabel(formLayoutWidget_2);
        temperatureLabel_5->setObjectName(QStringLiteral("temperatureLabel_5"));
        temperatureLabel_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_5);

        temperatureLineEdit_5 = new QLineEdit(formLayoutWidget_2);
        temperatureLineEdit_5->setObjectName(QStringLiteral("temperatureLineEdit_5"));
        temperatureLineEdit_5->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau5_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_5);

        Panneau9 = new QGroupBox(scrollAreaWidgetContents);
        Panneau9->setObjectName(QStringLiteral("Panneau9"));
        Panneau9->setGeometry(QRect(0, 300, 171, 151));
        Panneau9->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau9->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_3 = new QWidget(Panneau9);
        formLayoutWidget_3->setObjectName(QStringLiteral("formLayoutWidget_3"));
        formLayoutWidget_3->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_3->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau9_form = new QFormLayout(formLayoutWidget_3);
        Panneau9_form->setObjectName(QStringLiteral("Panneau9_form"));
        Panneau9_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau9_form->setLabelAlignment(Qt::AlignCenter);
        Panneau9_form->setVerticalSpacing(2);
        Panneau9_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_9 = new QLabel(formLayoutWidget_3);
        intensitLabel_9->setObjectName(QStringLiteral("intensitLabel_9"));
        intensitLabel_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_9);

        intensitLineEdit_9 = new QLineEdit(formLayoutWidget_3);
        intensitLineEdit_9->setObjectName(QStringLiteral("intensitLineEdit_9"));
        intensitLineEdit_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_9);

        tensionLabel_9 = new QLabel(formLayoutWidget_3);
        tensionLabel_9->setObjectName(QStringLiteral("tensionLabel_9"));
        tensionLabel_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_9);

        tensionLineEdit_9 = new QLineEdit(formLayoutWidget_3);
        tensionLineEdit_9->setObjectName(QStringLiteral("tensionLineEdit_9"));
        tensionLineEdit_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_9);

        energieLabel_9 = new QLabel(formLayoutWidget_3);
        energieLabel_9->setObjectName(QStringLiteral("energieLabel_9"));
        energieLabel_9->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_9->setAlignment(Qt::AlignCenter);

        Panneau9_form->setWidget(2, QFormLayout::LabelRole, energieLabel_9);

        energieLineEdit_9 = new QLineEdit(formLayoutWidget_3);
        energieLineEdit_9->setObjectName(QStringLiteral("energieLineEdit_9"));
        energieLineEdit_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_9);

        temperatureLabel_9 = new QLabel(formLayoutWidget_3);
        temperatureLabel_9->setObjectName(QStringLiteral("temperatureLabel_9"));
        temperatureLabel_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_9);

        temperatureLineEdit_9 = new QLineEdit(formLayoutWidget_3);
        temperatureLineEdit_9->setObjectName(QStringLiteral("temperatureLineEdit_9"));
        temperatureLineEdit_9->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau9_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_9);

        Panneau13 = new QGroupBox(scrollAreaWidgetContents);
        Panneau13->setObjectName(QStringLiteral("Panneau13"));
        Panneau13->setGeometry(QRect(0, 460, 171, 151));
        Panneau13->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau13->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_4 = new QWidget(Panneau13);
        formLayoutWidget_4->setObjectName(QStringLiteral("formLayoutWidget_4"));
        formLayoutWidget_4->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_4->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau13_form = new QFormLayout(formLayoutWidget_4);
        Panneau13_form->setObjectName(QStringLiteral("Panneau13_form"));
        Panneau13_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau13_form->setLabelAlignment(Qt::AlignCenter);
        Panneau13_form->setVerticalSpacing(2);
        Panneau13_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_13 = new QLabel(formLayoutWidget_4);
        intensitLabel_13->setObjectName(QStringLiteral("intensitLabel_13"));
        intensitLabel_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_13);

        intensitLineEdit_13 = new QLineEdit(formLayoutWidget_4);
        intensitLineEdit_13->setObjectName(QStringLiteral("intensitLineEdit_13"));
        intensitLineEdit_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_13);

        tensionLabel_13 = new QLabel(formLayoutWidget_4);
        tensionLabel_13->setObjectName(QStringLiteral("tensionLabel_13"));
        tensionLabel_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_13);

        tensionLineEdit_13 = new QLineEdit(formLayoutWidget_4);
        tensionLineEdit_13->setObjectName(QStringLiteral("tensionLineEdit_13"));
        tensionLineEdit_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_13);

        energieLabel_13 = new QLabel(formLayoutWidget_4);
        energieLabel_13->setObjectName(QStringLiteral("energieLabel_13"));
        energieLabel_13->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_13->setAlignment(Qt::AlignCenter);

        Panneau13_form->setWidget(2, QFormLayout::LabelRole, energieLabel_13);

        energieLineEdit_13 = new QLineEdit(formLayoutWidget_4);
        energieLineEdit_13->setObjectName(QStringLiteral("energieLineEdit_13"));
        energieLineEdit_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_13);

        temperatureLabel_13 = new QLabel(formLayoutWidget_4);
        temperatureLabel_13->setObjectName(QStringLiteral("temperatureLabel_13"));
        temperatureLabel_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_13);

        temperatureLineEdit_13 = new QLineEdit(formLayoutWidget_4);
        temperatureLineEdit_13->setObjectName(QStringLiteral("temperatureLineEdit_13"));
        temperatureLineEdit_13->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau13_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_13);

        Panneau6 = new QGroupBox(scrollAreaWidgetContents);
        Panneau6->setObjectName(QStringLiteral("Panneau6"));
        Panneau6->setGeometry(QRect(170, 150, 171, 151));
        Panneau6->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau6->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_8 = new QWidget(Panneau6);
        formLayoutWidget_8->setObjectName(QStringLiteral("formLayoutWidget_8"));
        formLayoutWidget_8->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_8->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau6_form = new QFormLayout(formLayoutWidget_8);
        Panneau6_form->setObjectName(QStringLiteral("Panneau6_form"));
        Panneau6_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau6_form->setLabelAlignment(Qt::AlignCenter);
        Panneau6_form->setVerticalSpacing(2);
        Panneau6_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_6 = new QLabel(formLayoutWidget_8);
        intensitLabel_6->setObjectName(QStringLiteral("intensitLabel_6"));
        intensitLabel_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_6);

        intensitLineEdit_6 = new QLineEdit(formLayoutWidget_8);
        intensitLineEdit_6->setObjectName(QStringLiteral("intensitLineEdit_6"));
        intensitLineEdit_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_6);

        tensionLabel_6 = new QLabel(formLayoutWidget_8);
        tensionLabel_6->setObjectName(QStringLiteral("tensionLabel_6"));
        tensionLabel_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_6);

        tensionLineEdit_6 = new QLineEdit(formLayoutWidget_8);
        tensionLineEdit_6->setObjectName(QStringLiteral("tensionLineEdit_6"));
        tensionLineEdit_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_6);

        energieLabel_6 = new QLabel(formLayoutWidget_8);
        energieLabel_6->setObjectName(QStringLiteral("energieLabel_6"));
        energieLabel_6->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_6->setAlignment(Qt::AlignCenter);

        Panneau6_form->setWidget(2, QFormLayout::LabelRole, energieLabel_6);

        energieLineEdit_6 = new QLineEdit(formLayoutWidget_8);
        energieLineEdit_6->setObjectName(QStringLiteral("energieLineEdit_6"));
        energieLineEdit_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_6);

        temperatureLabel_6 = new QLabel(formLayoutWidget_8);
        temperatureLabel_6->setObjectName(QStringLiteral("temperatureLabel_6"));
        temperatureLabel_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_6);

        temperatureLineEdit_6 = new QLineEdit(formLayoutWidget_8);
        temperatureLineEdit_6->setObjectName(QStringLiteral("temperatureLineEdit_6"));
        temperatureLineEdit_6->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau6_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_6);

        Panneau2 = new QGroupBox(scrollAreaWidgetContents);
        Panneau2->setObjectName(QStringLiteral("Panneau2"));
        Panneau2->setGeometry(QRect(170, 0, 171, 151));
        Panneau2->setContextMenuPolicy(Qt::NoContextMenu);
        formLayoutWidget_6 = new QWidget(Panneau2);
        formLayoutWidget_6->setObjectName(QStringLiteral("formLayoutWidget_6"));
        formLayoutWidget_6->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_6->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau2_form = new QFormLayout(formLayoutWidget_6);
        Panneau2_form->setObjectName(QStringLiteral("Panneau2_form"));
        Panneau2_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau2_form->setLabelAlignment(Qt::AlignCenter);
        Panneau2_form->setVerticalSpacing(2);
        Panneau2_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_2 = new QLabel(formLayoutWidget_6);
        intensitLabel_2->setObjectName(QStringLiteral("intensitLabel_2"));
        intensitLabel_2->setEnabled(true);
        intensitLabel_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_2);

        intensitLineEdit_2 = new QLineEdit(formLayoutWidget_6);
        intensitLineEdit_2->setObjectName(QStringLiteral("intensitLineEdit_2"));
        intensitLineEdit_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_2);

        tensionLabel_2 = new QLabel(formLayoutWidget_6);
        tensionLabel_2->setObjectName(QStringLiteral("tensionLabel_2"));
        tensionLabel_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_2);

        tensionLineEdit_2 = new QLineEdit(formLayoutWidget_6);
        tensionLineEdit_2->setObjectName(QStringLiteral("tensionLineEdit_2"));
        tensionLineEdit_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_2);

        energieLabel_2 = new QLabel(formLayoutWidget_6);
        energieLabel_2->setObjectName(QStringLiteral("energieLabel_2"));
        energieLabel_2->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_2->setAlignment(Qt::AlignCenter);

        Panneau2_form->setWidget(2, QFormLayout::LabelRole, energieLabel_2);

        energieLineEdit_2 = new QLineEdit(formLayoutWidget_6);
        energieLineEdit_2->setObjectName(QStringLiteral("energieLineEdit_2"));
        energieLineEdit_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_2);

        temperatureLabel_2 = new QLabel(formLayoutWidget_6);
        temperatureLabel_2->setObjectName(QStringLiteral("temperatureLabel_2"));
        temperatureLabel_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_2);

        temperatureLineEdit_2 = new QLineEdit(formLayoutWidget_6);
        temperatureLineEdit_2->setObjectName(QStringLiteral("temperatureLineEdit_2"));
        temperatureLineEdit_2->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau2_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_2);

        Panneau10 = new QGroupBox(scrollAreaWidgetContents);
        Panneau10->setObjectName(QStringLiteral("Panneau10"));
        Panneau10->setGeometry(QRect(170, 300, 171, 151));
        Panneau10->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau10->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_5 = new QWidget(Panneau10);
        formLayoutWidget_5->setObjectName(QStringLiteral("formLayoutWidget_5"));
        formLayoutWidget_5->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_5->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau10_form = new QFormLayout(formLayoutWidget_5);
        Panneau10_form->setObjectName(QStringLiteral("Panneau10_form"));
        Panneau10_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau10_form->setLabelAlignment(Qt::AlignCenter);
        Panneau10_form->setVerticalSpacing(2);
        Panneau10_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_10 = new QLabel(formLayoutWidget_5);
        intensitLabel_10->setObjectName(QStringLiteral("intensitLabel_10"));
        intensitLabel_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_10);

        intensitLineEdit_10 = new QLineEdit(formLayoutWidget_5);
        intensitLineEdit_10->setObjectName(QStringLiteral("intensitLineEdit_10"));
        intensitLineEdit_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_10);

        tensionLabel_10 = new QLabel(formLayoutWidget_5);
        tensionLabel_10->setObjectName(QStringLiteral("tensionLabel_10"));
        tensionLabel_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_10);

        tensionLineEdit_10 = new QLineEdit(formLayoutWidget_5);
        tensionLineEdit_10->setObjectName(QStringLiteral("tensionLineEdit_10"));
        tensionLineEdit_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_10);

        energieLabel_10 = new QLabel(formLayoutWidget_5);
        energieLabel_10->setObjectName(QStringLiteral("energieLabel_10"));
        energieLabel_10->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_10->setAlignment(Qt::AlignCenter);

        Panneau10_form->setWidget(2, QFormLayout::LabelRole, energieLabel_10);

        energieLineEdit_10 = new QLineEdit(formLayoutWidget_5);
        energieLineEdit_10->setObjectName(QStringLiteral("energieLineEdit_10"));
        energieLineEdit_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_10);

        temperatureLabel_10 = new QLabel(formLayoutWidget_5);
        temperatureLabel_10->setObjectName(QStringLiteral("temperatureLabel_10"));
        temperatureLabel_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_10);

        temperatureLineEdit_10 = new QLineEdit(formLayoutWidget_5);
        temperatureLineEdit_10->setObjectName(QStringLiteral("temperatureLineEdit_10"));
        temperatureLineEdit_10->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau10_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_10);

        Panneau14 = new QGroupBox(scrollAreaWidgetContents);
        Panneau14->setObjectName(QStringLiteral("Panneau14"));
        Panneau14->setGeometry(QRect(170, 460, 171, 151));
        Panneau14->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau14->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_7 = new QWidget(Panneau14);
        formLayoutWidget_7->setObjectName(QStringLiteral("formLayoutWidget_7"));
        formLayoutWidget_7->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_7->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau14_form = new QFormLayout(formLayoutWidget_7);
        Panneau14_form->setObjectName(QStringLiteral("Panneau14_form"));
        Panneau14_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau14_form->setLabelAlignment(Qt::AlignCenter);
        Panneau14_form->setVerticalSpacing(2);
        Panneau14_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_14 = new QLabel(formLayoutWidget_7);
        intensitLabel_14->setObjectName(QStringLiteral("intensitLabel_14"));
        intensitLabel_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_14);

        intensitLineEdit_14 = new QLineEdit(formLayoutWidget_7);
        intensitLineEdit_14->setObjectName(QStringLiteral("intensitLineEdit_14"));
        intensitLineEdit_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_14);

        tensionLabel_14 = new QLabel(formLayoutWidget_7);
        tensionLabel_14->setObjectName(QStringLiteral("tensionLabel_14"));
        tensionLabel_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_14);

        tensionLineEdit_14 = new QLineEdit(formLayoutWidget_7);
        tensionLineEdit_14->setObjectName(QStringLiteral("tensionLineEdit_14"));
        tensionLineEdit_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_14);

        energieLabel_14 = new QLabel(formLayoutWidget_7);
        energieLabel_14->setObjectName(QStringLiteral("energieLabel_14"));
        energieLabel_14->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_14->setAlignment(Qt::AlignCenter);

        Panneau14_form->setWidget(2, QFormLayout::LabelRole, energieLabel_14);

        energieLineEdit_14 = new QLineEdit(formLayoutWidget_7);
        energieLineEdit_14->setObjectName(QStringLiteral("energieLineEdit_14"));
        energieLineEdit_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_14);

        temperatureLabel_14 = new QLabel(formLayoutWidget_7);
        temperatureLabel_14->setObjectName(QStringLiteral("temperatureLabel_14"));
        temperatureLabel_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_14);

        temperatureLineEdit_14 = new QLineEdit(formLayoutWidget_7);
        temperatureLineEdit_14->setObjectName(QStringLiteral("temperatureLineEdit_14"));
        temperatureLineEdit_14->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau14_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_14);

        Panneau11 = new QGroupBox(scrollAreaWidgetContents);
        Panneau11->setObjectName(QStringLiteral("Panneau11"));
        Panneau11->setGeometry(QRect(340, 300, 171, 151));
        Panneau11->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau11->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_9 = new QWidget(Panneau11);
        formLayoutWidget_9->setObjectName(QStringLiteral("formLayoutWidget_9"));
        formLayoutWidget_9->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_9->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau11_form = new QFormLayout(formLayoutWidget_9);
        Panneau11_form->setObjectName(QStringLiteral("Panneau11_form"));
        Panneau11_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau11_form->setLabelAlignment(Qt::AlignCenter);
        Panneau11_form->setVerticalSpacing(2);
        Panneau11_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_11 = new QLabel(formLayoutWidget_9);
        intensitLabel_11->setObjectName(QStringLiteral("intensitLabel_11"));
        intensitLabel_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_11);

        intensitLineEdit_11 = new QLineEdit(formLayoutWidget_9);
        intensitLineEdit_11->setObjectName(QStringLiteral("intensitLineEdit_11"));
        intensitLineEdit_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_11);

        tensionLabel_11 = new QLabel(formLayoutWidget_9);
        tensionLabel_11->setObjectName(QStringLiteral("tensionLabel_11"));
        tensionLabel_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_11);

        tensionLineEdit_11 = new QLineEdit(formLayoutWidget_9);
        tensionLineEdit_11->setObjectName(QStringLiteral("tensionLineEdit_11"));
        tensionLineEdit_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_11);

        energieLabel_11 = new QLabel(formLayoutWidget_9);
        energieLabel_11->setObjectName(QStringLiteral("energieLabel_11"));
        energieLabel_11->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_11->setAlignment(Qt::AlignCenter);

        Panneau11_form->setWidget(2, QFormLayout::LabelRole, energieLabel_11);

        energieLineEdit_11 = new QLineEdit(formLayoutWidget_9);
        energieLineEdit_11->setObjectName(QStringLiteral("energieLineEdit_11"));
        energieLineEdit_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_11);

        temperatureLabel_11 = new QLabel(formLayoutWidget_9);
        temperatureLabel_11->setObjectName(QStringLiteral("temperatureLabel_11"));
        temperatureLabel_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_11);

        temperatureLineEdit_11 = new QLineEdit(formLayoutWidget_9);
        temperatureLineEdit_11->setObjectName(QStringLiteral("temperatureLineEdit_11"));
        temperatureLineEdit_11->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau11_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_11);

        Panneau3 = new QGroupBox(scrollAreaWidgetContents);
        Panneau3->setObjectName(QStringLiteral("Panneau3"));
        Panneau3->setGeometry(QRect(340, 0, 171, 151));
        Panneau3->setContextMenuPolicy(Qt::NoContextMenu);
        formLayoutWidget_10 = new QWidget(Panneau3);
        formLayoutWidget_10->setObjectName(QStringLiteral("formLayoutWidget_10"));
        formLayoutWidget_10->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_10->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau3_form = new QFormLayout(formLayoutWidget_10);
        Panneau3_form->setObjectName(QStringLiteral("Panneau3_form"));
        Panneau3_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau3_form->setLabelAlignment(Qt::AlignCenter);
        Panneau3_form->setVerticalSpacing(2);
        Panneau3_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_3 = new QLabel(formLayoutWidget_10);
        intensitLabel_3->setObjectName(QStringLiteral("intensitLabel_3"));
        intensitLabel_3->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau3_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_3);

        intensitLineEdit_3 = new QLineEdit(formLayoutWidget_10);
        intensitLineEdit_3->setObjectName(QStringLiteral("intensitLineEdit_3"));
        intensitLineEdit_3->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau3_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_3);

        tensionLabel_3 = new QLabel(formLayoutWidget_10);
        tensionLabel_3->setObjectName(QStringLiteral("tensionLabel_3"));
        tensionLabel_3->setContextMenuPolicy(Qt::NoContextMenu);
        tensionLabel_3->setTextInteractionFlags(Qt::NoTextInteraction);

        Panneau3_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_3);

        tensionLineEdit_3 = new QLineEdit(formLayoutWidget_10);
        tensionLineEdit_3->setObjectName(QStringLiteral("tensionLineEdit_3"));
        tensionLineEdit_3->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau3_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_3);

        energieLabel_3 = new QLabel(formLayoutWidget_10);
        energieLabel_3->setObjectName(QStringLiteral("energieLabel_3"));
        energieLabel_3->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_3->setAlignment(Qt::AlignCenter);

        Panneau3_form->setWidget(2, QFormLayout::LabelRole, energieLabel_3);

        energieLineEdit_3 = new QLineEdit(formLayoutWidget_10);
        energieLineEdit_3->setObjectName(QStringLiteral("energieLineEdit_3"));
        energieLineEdit_3->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau3_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_3);

        temperatureLabel_3 = new QLabel(formLayoutWidget_10);
        temperatureLabel_3->setObjectName(QStringLiteral("temperatureLabel_3"));
        temperatureLabel_3->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau3_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_3);

        temperatureLineEdit_3 = new QLineEdit(formLayoutWidget_10);
        temperatureLineEdit_3->setObjectName(QStringLiteral("temperatureLineEdit_3"));
        temperatureLineEdit_3->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau3_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_3);

        Panneau15 = new QGroupBox(scrollAreaWidgetContents);
        Panneau15->setObjectName(QStringLiteral("Panneau15"));
        Panneau15->setGeometry(QRect(340, 460, 171, 151));
        Panneau15->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau15->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_11 = new QWidget(Panneau15);
        formLayoutWidget_11->setObjectName(QStringLiteral("formLayoutWidget_11"));
        formLayoutWidget_11->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_11->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau15_form = new QFormLayout(formLayoutWidget_11);
        Panneau15_form->setObjectName(QStringLiteral("Panneau15_form"));
        Panneau15_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau15_form->setLabelAlignment(Qt::AlignCenter);
        Panneau15_form->setVerticalSpacing(2);
        Panneau15_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_15 = new QLabel(formLayoutWidget_11);
        intensitLabel_15->setObjectName(QStringLiteral("intensitLabel_15"));
        intensitLabel_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_15);

        intensitLineEdit_15 = new QLineEdit(formLayoutWidget_11);
        intensitLineEdit_15->setObjectName(QStringLiteral("intensitLineEdit_15"));
        intensitLineEdit_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_15);

        tensionLabel_15 = new QLabel(formLayoutWidget_11);
        tensionLabel_15->setObjectName(QStringLiteral("tensionLabel_15"));
        tensionLabel_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_15);

        tensionLineEdit_15 = new QLineEdit(formLayoutWidget_11);
        tensionLineEdit_15->setObjectName(QStringLiteral("tensionLineEdit_15"));
        tensionLineEdit_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_15);

        energieLabel_15 = new QLabel(formLayoutWidget_11);
        energieLabel_15->setObjectName(QStringLiteral("energieLabel_15"));
        energieLabel_15->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_15->setAlignment(Qt::AlignCenter);

        Panneau15_form->setWidget(2, QFormLayout::LabelRole, energieLabel_15);

        energieLineEdit_15 = new QLineEdit(formLayoutWidget_11);
        energieLineEdit_15->setObjectName(QStringLiteral("energieLineEdit_15"));
        energieLineEdit_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_15);

        temperatureLabel_15 = new QLabel(formLayoutWidget_11);
        temperatureLabel_15->setObjectName(QStringLiteral("temperatureLabel_15"));
        temperatureLabel_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_15);

        temperatureLineEdit_15 = new QLineEdit(formLayoutWidget_11);
        temperatureLineEdit_15->setObjectName(QStringLiteral("temperatureLineEdit_15"));
        temperatureLineEdit_15->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau15_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_15);

        Panneau7 = new QGroupBox(scrollAreaWidgetContents);
        Panneau7->setObjectName(QStringLiteral("Panneau7"));
        Panneau7->setGeometry(QRect(340, 150, 171, 151));
        Panneau7->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau7->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_12 = new QWidget(Panneau7);
        formLayoutWidget_12->setObjectName(QStringLiteral("formLayoutWidget_12"));
        formLayoutWidget_12->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_12->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau7_form = new QFormLayout(formLayoutWidget_12);
        Panneau7_form->setObjectName(QStringLiteral("Panneau7_form"));
        Panneau7_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau7_form->setLabelAlignment(Qt::AlignCenter);
        Panneau7_form->setVerticalSpacing(2);
        Panneau7_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_7 = new QLabel(formLayoutWidget_12);
        intensitLabel_7->setObjectName(QStringLiteral("intensitLabel_7"));
        intensitLabel_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_7);

        intensitLineEdit_7 = new QLineEdit(formLayoutWidget_12);
        intensitLineEdit_7->setObjectName(QStringLiteral("intensitLineEdit_7"));
        intensitLineEdit_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_7);

        tensionLabel_7 = new QLabel(formLayoutWidget_12);
        tensionLabel_7->setObjectName(QStringLiteral("tensionLabel_7"));
        tensionLabel_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_7);

        tensionLineEdit_7 = new QLineEdit(formLayoutWidget_12);
        tensionLineEdit_7->setObjectName(QStringLiteral("tensionLineEdit_7"));
        tensionLineEdit_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_7);

        energieLabel_7 = new QLabel(formLayoutWidget_12);
        energieLabel_7->setObjectName(QStringLiteral("energieLabel_7"));
        energieLabel_7->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_7->setAlignment(Qt::AlignCenter);

        Panneau7_form->setWidget(2, QFormLayout::LabelRole, energieLabel_7);

        energieLineEdit_7 = new QLineEdit(formLayoutWidget_12);
        energieLineEdit_7->setObjectName(QStringLiteral("energieLineEdit_7"));
        energieLineEdit_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_7);

        temperatureLabel_7 = new QLabel(formLayoutWidget_12);
        temperatureLabel_7->setObjectName(QStringLiteral("temperatureLabel_7"));
        temperatureLabel_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_7);

        temperatureLineEdit_7 = new QLineEdit(formLayoutWidget_12);
        temperatureLineEdit_7->setObjectName(QStringLiteral("temperatureLineEdit_7"));
        temperatureLineEdit_7->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau7_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_7);

        Panneau12 = new QGroupBox(scrollAreaWidgetContents);
        Panneau12->setObjectName(QStringLiteral("Panneau12"));
        Panneau12->setGeometry(QRect(510, 300, 171, 151));
        Panneau12->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau12->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_13 = new QWidget(Panneau12);
        formLayoutWidget_13->setObjectName(QStringLiteral("formLayoutWidget_13"));
        formLayoutWidget_13->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_13->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau12_form = new QFormLayout(formLayoutWidget_13);
        Panneau12_form->setObjectName(QStringLiteral("Panneau12_form"));
        Panneau12_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau12_form->setLabelAlignment(Qt::AlignCenter);
        Panneau12_form->setVerticalSpacing(2);
        Panneau12_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_12 = new QLabel(formLayoutWidget_13);
        intensitLabel_12->setObjectName(QStringLiteral("intensitLabel_12"));
        intensitLabel_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_12);

        intensitLineEdit_12 = new QLineEdit(formLayoutWidget_13);
        intensitLineEdit_12->setObjectName(QStringLiteral("intensitLineEdit_12"));
        intensitLineEdit_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_12);

        tensionLabel_12 = new QLabel(formLayoutWidget_13);
        tensionLabel_12->setObjectName(QStringLiteral("tensionLabel_12"));
        tensionLabel_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_12);

        tensionLineEdit_12 = new QLineEdit(formLayoutWidget_13);
        tensionLineEdit_12->setObjectName(QStringLiteral("tensionLineEdit_12"));
        tensionLineEdit_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_12);

        energieLabel_12 = new QLabel(formLayoutWidget_13);
        energieLabel_12->setObjectName(QStringLiteral("energieLabel_12"));
        energieLabel_12->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_12->setAlignment(Qt::AlignCenter);

        Panneau12_form->setWidget(2, QFormLayout::LabelRole, energieLabel_12);

        energieLineEdit_12 = new QLineEdit(formLayoutWidget_13);
        energieLineEdit_12->setObjectName(QStringLiteral("energieLineEdit_12"));
        energieLineEdit_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_12);

        temperatureLabel_12 = new QLabel(formLayoutWidget_13);
        temperatureLabel_12->setObjectName(QStringLiteral("temperatureLabel_12"));
        temperatureLabel_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_12);

        temperatureLineEdit_12 = new QLineEdit(formLayoutWidget_13);
        temperatureLineEdit_12->setObjectName(QStringLiteral("temperatureLineEdit_12"));
        temperatureLineEdit_12->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau12_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_12);

        Panneau4 = new QGroupBox(scrollAreaWidgetContents);
        Panneau4->setObjectName(QStringLiteral("Panneau4"));
        Panneau4->setGeometry(QRect(510, 0, 171, 151));
        Panneau4->setContextMenuPolicy(Qt::NoContextMenu);
        formLayoutWidget_14 = new QWidget(Panneau4);
        formLayoutWidget_14->setObjectName(QStringLiteral("formLayoutWidget_14"));
        formLayoutWidget_14->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_14->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau4_form = new QFormLayout(formLayoutWidget_14);
        Panneau4_form->setObjectName(QStringLiteral("Panneau4_form"));
        Panneau4_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau4_form->setLabelAlignment(Qt::AlignCenter);
        Panneau4_form->setVerticalSpacing(2);
        Panneau4_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_4 = new QLabel(formLayoutWidget_14);
        intensitLabel_4->setObjectName(QStringLiteral("intensitLabel_4"));
        intensitLabel_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_4);

        intensitLineEdit_4 = new QLineEdit(formLayoutWidget_14);
        intensitLineEdit_4->setObjectName(QStringLiteral("intensitLineEdit_4"));
        intensitLineEdit_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_4);

        tensionLabel_4 = new QLabel(formLayoutWidget_14);
        tensionLabel_4->setObjectName(QStringLiteral("tensionLabel_4"));
        tensionLabel_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_4);

        tensionLineEdit_4 = new QLineEdit(formLayoutWidget_14);
        tensionLineEdit_4->setObjectName(QStringLiteral("tensionLineEdit_4"));
        tensionLineEdit_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_4);

        energieLabel_4 = new QLabel(formLayoutWidget_14);
        energieLabel_4->setObjectName(QStringLiteral("energieLabel_4"));
        energieLabel_4->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_4->setAlignment(Qt::AlignCenter);

        Panneau4_form->setWidget(2, QFormLayout::LabelRole, energieLabel_4);

        energieLineEdit_4 = new QLineEdit(formLayoutWidget_14);
        energieLineEdit_4->setObjectName(QStringLiteral("energieLineEdit_4"));
        energieLineEdit_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_4);

        temperatureLabel_4 = new QLabel(formLayoutWidget_14);
        temperatureLabel_4->setObjectName(QStringLiteral("temperatureLabel_4"));
        temperatureLabel_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_4);

        temperatureLineEdit_4 = new QLineEdit(formLayoutWidget_14);
        temperatureLineEdit_4->setObjectName(QStringLiteral("temperatureLineEdit_4"));
        temperatureLineEdit_4->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau4_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_4);

        Panneau16 = new QGroupBox(scrollAreaWidgetContents);
        Panneau16->setObjectName(QStringLiteral("Panneau16"));
        Panneau16->setGeometry(QRect(510, 460, 171, 151));
        Panneau16->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau16->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_15 = new QWidget(Panneau16);
        formLayoutWidget_15->setObjectName(QStringLiteral("formLayoutWidget_15"));
        formLayoutWidget_15->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_15->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau16_form = new QFormLayout(formLayoutWidget_15);
        Panneau16_form->setObjectName(QStringLiteral("Panneau16_form"));
        Panneau16_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau16_form->setLabelAlignment(Qt::AlignCenter);
        Panneau16_form->setVerticalSpacing(2);
        Panneau16_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_16 = new QLabel(formLayoutWidget_15);
        intensitLabel_16->setObjectName(QStringLiteral("intensitLabel_16"));
        intensitLabel_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_16);

        intensitLineEdit_16 = new QLineEdit(formLayoutWidget_15);
        intensitLineEdit_16->setObjectName(QStringLiteral("intensitLineEdit_16"));
        intensitLineEdit_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_16);

        tensionLabel_16 = new QLabel(formLayoutWidget_15);
        tensionLabel_16->setObjectName(QStringLiteral("tensionLabel_16"));
        tensionLabel_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_16);

        tensionLineEdit_16 = new QLineEdit(formLayoutWidget_15);
        tensionLineEdit_16->setObjectName(QStringLiteral("tensionLineEdit_16"));
        tensionLineEdit_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_16);

        energieLabel_16 = new QLabel(formLayoutWidget_15);
        energieLabel_16->setObjectName(QStringLiteral("energieLabel_16"));
        energieLabel_16->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_16->setAlignment(Qt::AlignCenter);

        Panneau16_form->setWidget(2, QFormLayout::LabelRole, energieLabel_16);

        energieLineEdit_16 = new QLineEdit(formLayoutWidget_15);
        energieLineEdit_16->setObjectName(QStringLiteral("energieLineEdit_16"));
        energieLineEdit_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_16);

        temperatureLabel_16 = new QLabel(formLayoutWidget_15);
        temperatureLabel_16->setObjectName(QStringLiteral("temperatureLabel_16"));
        temperatureLabel_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_16);

        temperatureLineEdit_16 = new QLineEdit(formLayoutWidget_15);
        temperatureLineEdit_16->setObjectName(QStringLiteral("temperatureLineEdit_16"));
        temperatureLineEdit_16->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau16_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_16);

        Panneau8 = new QGroupBox(scrollAreaWidgetContents);
        Panneau8->setObjectName(QStringLiteral("Panneau8"));
        Panneau8->setGeometry(QRect(510, 150, 171, 151));
        Panneau8->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau8->setStyleSheet(QStringLiteral(""));
        formLayoutWidget_16 = new QWidget(Panneau8);
        formLayoutWidget_16->setObjectName(QStringLiteral("formLayoutWidget_16"));
        formLayoutWidget_16->setGeometry(QRect(0, 20, 160, 121));
        formLayoutWidget_16->setContextMenuPolicy(Qt::NoContextMenu);
        Panneau8_form = new QFormLayout(formLayoutWidget_16);
        Panneau8_form->setObjectName(QStringLiteral("Panneau8_form"));
        Panneau8_form->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        Panneau8_form->setLabelAlignment(Qt::AlignCenter);
        Panneau8_form->setVerticalSpacing(2);
        Panneau8_form->setContentsMargins(0, 0, 0, 0);
        intensitLabel_8 = new QLabel(formLayoutWidget_16);
        intensitLabel_8->setObjectName(QStringLiteral("intensitLabel_8"));
        intensitLabel_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(0, QFormLayout::LabelRole, intensitLabel_8);

        intensitLineEdit_8 = new QLineEdit(formLayoutWidget_16);
        intensitLineEdit_8->setObjectName(QStringLiteral("intensitLineEdit_8"));
        intensitLineEdit_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(0, QFormLayout::FieldRole, intensitLineEdit_8);

        tensionLabel_8 = new QLabel(formLayoutWidget_16);
        tensionLabel_8->setObjectName(QStringLiteral("tensionLabel_8"));
        tensionLabel_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(1, QFormLayout::LabelRole, tensionLabel_8);

        tensionLineEdit_8 = new QLineEdit(formLayoutWidget_16);
        tensionLineEdit_8->setObjectName(QStringLiteral("tensionLineEdit_8"));
        tensionLineEdit_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(1, QFormLayout::FieldRole, tensionLineEdit_8);

        energieLabel_8 = new QLabel(formLayoutWidget_16);
        energieLabel_8->setObjectName(QStringLiteral("energieLabel_8"));
        energieLabel_8->setContextMenuPolicy(Qt::NoContextMenu);
        energieLabel_8->setAlignment(Qt::AlignCenter);

        Panneau8_form->setWidget(2, QFormLayout::LabelRole, energieLabel_8);

        energieLineEdit_8 = new QLineEdit(formLayoutWidget_16);
        energieLineEdit_8->setObjectName(QStringLiteral("energieLineEdit_8"));
        energieLineEdit_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(2, QFormLayout::FieldRole, energieLineEdit_8);

        temperatureLabel_8 = new QLabel(formLayoutWidget_16);
        temperatureLabel_8->setObjectName(QStringLiteral("temperatureLabel_8"));
        temperatureLabel_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(3, QFormLayout::LabelRole, temperatureLabel_8);

        temperatureLineEdit_8 = new QLineEdit(formLayoutWidget_16);
        temperatureLineEdit_8->setObjectName(QStringLiteral("temperatureLineEdit_8"));
        temperatureLineEdit_8->setContextMenuPolicy(Qt::NoContextMenu);

        Panneau8_form->setWidget(3, QFormLayout::FieldRole, temperatureLineEdit_8);

        scrollArea->setWidget(scrollAreaWidgetContents);
        onglets->addTab(ong_donnees, QString());
        ong_defauts = new QWidget();
        ong_defauts->setObjectName(QStringLiteral("ong_defauts"));
        defauts = new QTextEdit(ong_defauts);
        defauts->setObjectName(QStringLiteral("defauts"));
        defauts->setGeometry(QRect(0, -10, 661, 331));
        onglets->addTab(ong_defauts, QString());
        MaisonBioDlg->setCentralWidget(centralwidget);

        retranslateUi(MaisonBioDlg);

        onglets->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MaisonBioDlg);
    } // setupUi

    void retranslateUi(QMainWindow *MaisonBioDlg)
    {
        MaisonBioDlg->setWindowTitle(QApplication::translate("MaisonBioDlg", "MainWindow", 0));
#ifndef QT_NO_TOOLTIP
        onglets->setToolTip(QApplication::translate("MaisonBioDlg", "<html><head/><body><p><br/></p></body></html>", 0));
#endif // QT_NO_TOOLTIP
        Panneau1->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 1", 0));
        intensitLabel_1->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_1->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_1->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_1->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau5->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 5", 0));
        intensitLabel_5->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_5->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_5->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_5->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau9->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 9", 0));
        intensitLabel_9->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_9->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_9->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_9->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau13->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 13", 0));
        intensitLabel_13->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_13->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_13->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_13->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau6->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 6", 0));
        intensitLabel_6->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_6->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_6->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_6->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau2->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 2", 0));
        intensitLabel_2->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_2->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_2->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_2->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau10->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 10", 0));
        intensitLabel_10->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_10->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_10->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_10->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau14->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 14", 0));
        intensitLabel_14->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_14->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_14->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_14->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau11->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 11", 0));
        intensitLabel_11->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_11->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_11->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_11->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau3->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 3", 0));
        intensitLabel_3->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
#ifndef QT_NO_TOOLTIP
        intensitLineEdit_3->setToolTip(QString());
#endif // QT_NO_TOOLTIP
        tensionLabel_3->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_3->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_3->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau15->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 15", 0));
        intensitLabel_15->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_15->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_15->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_15->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau7->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 7", 0));
        intensitLabel_7->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_7->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_7->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_7->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau12->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 12", 0));
        intensitLabel_12->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_12->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_12->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_12->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau4->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 4", 0));
        intensitLabel_4->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_4->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_4->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_4->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau16->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 16", 0));
        intensitLabel_16->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_16->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_16->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_16->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        Panneau8->setTitle(QApplication::translate("MaisonBioDlg", "Panneau 8", 0));
        intensitLabel_8->setText(QApplication::translate("MaisonBioDlg", "Intensit\303\251:", 0));
        tensionLabel_8->setText(QApplication::translate("MaisonBioDlg", "Tension:", 0));
        energieLabel_8->setText(QApplication::translate("MaisonBioDlg", "Energie:", 0));
        temperatureLabel_8->setText(QApplication::translate("MaisonBioDlg", "Temp:", 0));
        onglets->setTabText(onglets->indexOf(ong_donnees), QApplication::translate("MaisonBioDlg", "Donn\303\251es", 0));
        onglets->setTabText(onglets->indexOf(ong_defauts), QApplication::translate("MaisonBioDlg", "D\303\251fauts", 0));
    } // retranslateUi

};

namespace Ui {
    class MaisonBioDlg: public Ui_MaisonBioDlg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAISONBIODLG_H
