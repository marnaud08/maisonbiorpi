
/*
 *  Copyright (C) 2015  local  (local@appert44.org)
 *  @file         MaisonBioDlgTest.h
 *  Classe        MaisonBioDlg
 *  @note         Classe en charge des tests unitaires
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef _MAISONBIODLGTEST_H
#define _MAISONBIODLGTEST_H

// Includes system C

// Includes system C++
#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

// Includes qt

// Includes application
#include "MaisonBioDlg.h"

class MaisonBioDlgTest : public CppUnit::TestFixture
{
    CPPUNIT_TEST_SUITE(MaisonBioDlgTest);
    CPPUNIT_TEST(testMainView);
    CPPUNIT_TEST_SUITE_END();

public :
    void setUp();
    void tearDown();
    void testMainView();

    void testConstructor();
    void testUnitaire1();

protected :
    // Attributs proteges

    // Methode protegees

private :
    // Attributs prives

    // Methodes privees
};

#endif  // _MAISONBIODLGTEST_H


