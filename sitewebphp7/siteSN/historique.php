<?php
	include ("config/config.php");	// Inlusion du fichier config.php
?>
<html>
	<head>
		<?php
			include ("inc/head.php");	// Inlusion du fichier head.php
		?>
		<title>Historique</title>		<!-- titre de l'onglet -->
	</head>
	<body onload="init();">
		<?php
			include ("inc/menu.php");	// Inlusion du fichier menu.php
			include ("inc/header.php");	// Inlusion du fichier header.php
		?>
		<div class="div_valeur">
			<h4><u>Historique des valeurs:</u></h4>		<!-- Titre du rectangle blanc -->
			<div id="div_historique_form">
				</br>
				<form name='testform' method='post' action='view.php' style='margin:0px;'>		<!-- Début du formulaire -->
					<h4 class="titre_form">Choisissez un panneau :</h4>
					<?php
	    				// Variable qui ajoutera l'attribut selected de la liste déroulante
	    				$selected = '';
	    				// Parcours du tableau
	    				echo '<select id ="menu_deroulant" name="panneau">',"\n";
	    				for($i=1; $i<=16; $i+=1)
	    				{
	    				 // Définir la valeur par défaut 
	    				 if($i == 1)
	    				  {
	    				    $selected = ' selected="selected"';
	    				  }
	    				  // Affichage de la ligne
	    				  echo "\t",'<option value="', $i ,'"', $selected ,'> Panneau ', $i ,'</option>',"\n";
	    				  // Remise à zéro de $selected
	    				  $selected='';
	    				}
	    				echo '</select>',"\n";
	    			?>
					</br>

					<!-- FICHIER DE STYLE DU CALENDRIER -->
					<link rel='stylesheet' href='gnoocalendar.css' />
					<!-- FICHIER DE SCRIPT DU CALENDRIER -->
					<script type="text/javascript" src="gnoocalendar.js"></script>
					<!-- Début du srcipt -->
					<script type="text/javascript">
						<!--
						/******************************/
						self.defaultStatus = "GnooCalendar 1.4";
						/******************************/
						/* 
						* instanciation de l'objet
						*/
						var CL = new GnooCalendar("CL", 0, 5 );
						/******************************/
						function init()
						{
							CL.init("calend", document.forms["testform"].elements["testestformield1"]);
							CL.isDragable(true);
						}
						/******************************/
						//-->
						<?php $datedujour = date("Y-m-d");?>
					</script>
					<h4 class="titre_form">Choisissez une date :</h4>
					<?php 
						// Encare de la date avec la date du jour en valeur auto
						echo "<input id='date' type='text' name='testestformield1'  value='". $datedujour."' />";
					?>
					<!-- Bouton pour afficher le calendrier -->
					<input class='input_afficher' type='button' name='show' onclick='CL.show(); CL.setTitle(" "); CL.setFormat("fr");' value='Afficher le calendrier' />
					<div id="calend" style="position: absolute; top: 370px; left: 55%; width: 200px; height: 200px; z-index: 99; border: solid 0px #000000;visibility: hidden;">
						&nbsp;
					</div>
					<input id="input_Voir" type="submit" value="Voir"> <!-- Boutton voir--> 
				</form>
			</div>
		</div>
	</body>
</html>
